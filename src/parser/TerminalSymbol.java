package parser;

public class TerminalSymbol extends Symbol {	
	@Override
	public boolean isTerminal() {
		return true;
	}
	
	public TerminalSymbol(String symbol) {
		super(symbol);
	}
}

package parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * Class for representing
 *  a GNF Grammer
 *
 */
public class Grammar {
	public Grammar(List<NonTerminalSymbol> V,
				   List<TerminalSymbol> SIGMA,
				   NonTerminalSymbol S,
				   List<ProductionRule> P) {
		
		_V = V;
		_SIGMA = SIGMA;
		_S = S;
		_P = P;
	}
	
	public List<SymbolList> getProductionsWithSymbol(Symbol sym) {
		ArrayList<SymbolList> toRet = new ArrayList<>();
		
		for (ProductionRule p : _P) {
			if (p.from().equals(sym))
				toRet.add(p.to());
		}
		
		return toRet;
	}
	
	public List<NonTerminalSymbol> V() {
		return Collections.unmodifiableList(_V);
	}
	
	public List<TerminalSymbol> SIGMA() {
		return Collections.unmodifiableList(_SIGMA);
	}
	
	public NonTerminalSymbol S() {
		return _S;
	}
	
	public List<ProductionRule> P() {
		return Collections.unmodifiableList(_P);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("- Grammar\n");
		sb.append("Non-terminal symbols:");
		for (Symbol symb : _V) {
			sb.append(" " + symb.getSymbol());
		}
		sb.append("\nTerminal symbols:");
		for (Symbol symb : _SIGMA) {
			sb.append(" " + symb.getSymbol());
		}
		sb.append("\nInitial symbol: " + _S.getSymbol());
		sb.append("\nProductions:");
		for (ProductionRule pr : _P) {
			sb.append(" " + pr.from().getSymbol() + "->" + pr.to().toString());
		}
		
		return sb.toString();
	}
	
	private List<NonTerminalSymbol> _V;
	private List<TerminalSymbol> _SIGMA;
	private NonTerminalSymbol _S;
	private List<ProductionRule> _P;
}

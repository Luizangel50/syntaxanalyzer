package parser;

import java.util.Stack;

public class TopDownParser {

	public TopDownParser () {
	}
	
	public TopDownParser (Grammar g) {
		_grammar = g;
	}
	
	public boolean parse (String str) {
		_s = new Stack<>();
		
		_s.push(new SymbolList(_grammar.S()));
		while (!_s.empty()) {
			SymbolList curr = _s.peek();
			_s.pop();
			
			if (!curr.hasNonTerminalSymbol() && 
					curr.equalsWithString(str))
				return true;
			
			if (curr.hasNonTerminalSymbol()) {
				NonTerminalSymbol nts = curr.firstNonTerminalSymbol();
				
				for (SymbolList sList: _grammar.getProductionsWithSymbol(nts)) {
					SymbolList copy = new SymbolList(curr);
					copy.substituteNonTerminalSymbol(nts, sList);
					if (hasSamePrefix(copy, str)) {
						_s.push(copy);
					}
				}
			}
			
		}
		return false;
	}
	
	private boolean hasSamePrefix(SymbolList symbols, String str) {
		int symbolsSize = symbols.prefix().length();
		
		if (symbolsSize == 0)
			return true;
		
		return symbolsSize == 0 || 
			   str.substring(0, symbolsSize).equals(symbols.prefix());
	}
	
	public Grammar getGrammer() {
		return _grammar;
	}
	
	public void setGrammar(Grammar g) {
		_grammar = g;
	}
	
	private Grammar _grammar;
	private Stack<SymbolList> _s;
}

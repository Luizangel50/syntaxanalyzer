package parser;

public abstract class Symbol {
	public abstract boolean isTerminal();

	public Symbol(String symbol) {
		_symbol = symbol;
	}

	public String getSymbol() {
		return _symbol;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Symbol))
			return false;
		if (obj == this)
			return true;

		Symbol rhs = (Symbol) obj;
		return _symbol.equals(rhs._symbol);
	}

	protected String _symbol;
}

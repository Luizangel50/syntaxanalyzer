package parser;

public class ProductionRule {
	
	public ProductionRule(Symbol from, SymbolList to) {
		_from = from;
		_to = to;
	}
	
	public Symbol from() {
		return _from;
	}
	
	public SymbolList to() {
		return _to;
	}
	
	private Symbol _from;
	private SymbolList _to;
}

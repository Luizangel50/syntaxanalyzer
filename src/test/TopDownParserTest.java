package test;

import static org.junit.Assert.*;

import org.junit.Test;

import parser.Grammar;
import parser.TopDownParser;
import utils.GrammarCreator;
import utils.SimpleTextFile;

public class TopDownParserTest {
	
	@Test
	public void test1() throws Exception {
		SimpleTextFile file = new SimpleTextFile("src/test/test1.txt");
		GrammarCreator gc = new GrammarCreator(file.read());
		Grammar g = gc.create();
		TopDownParser tdp = new TopDownParser(g);
		
		assertTrue(tdp.parse("a"));
		assertFalse(tdp.parse("ab"));
	}
	
	@Test
	public void test2() throws Exception {
		SimpleTextFile file = new SimpleTextFile("src/test/test2.txt");
		GrammarCreator gc = new GrammarCreator(file.read());
		Grammar g = gc.create();
		TopDownParser tdp = new TopDownParser(g);
		
		assertTrue(tdp.parse("(b)"));
		assertTrue(tdp.parse("(b+b)"));
	}
	
}

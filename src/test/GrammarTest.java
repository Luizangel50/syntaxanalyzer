package test;

import static org.junit.Assert.*;

import org.junit.Test;

import parser.Grammar;
import utils.GrammarCreator;
import utils.SimpleTextFile;

public class GrammarTest {
	@Test
	public void testGrammarCreation1() throws Exception {
		SimpleTextFile file = new SimpleTextFile("src/test/test1.txt");
		GrammarCreator gc = new GrammarCreator(file.read());
		Grammar g = gc.create();		
		assertEquals(g.toString(),"- Grammar\n"
				+ "Non-terminal symbols: S A B C D\n"
				+ "Terminal symbols: a b c d\n"
				+ "Initial symbol: A\n"
				+ "Productions: A->a B->b");		
	}
	
	@Test
	public void testGrammarCreation2() throws Exception {
		SimpleTextFile file = new SimpleTextFile("src/test/test2.txt");
		GrammarCreator gc = new GrammarCreator(file.read());
		Grammar g = gc.create();
		
		assertEquals(g.toString(),"- Grammar"
				+ "Non-terminal symbols: S A B C D T\n"
				+ "Terminal symbols: b + ( )\n"
				+ "Initial symbol: S\n"
				+ "Productions: S->A A->T T->(BC C->) B->b C->+D D->) D->b)");		
	}
}

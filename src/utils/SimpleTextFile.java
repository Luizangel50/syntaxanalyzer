package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class SimpleTextFile implements IReadable {

	public SimpleTextFile(String fileName) {
		_fileName = fileName;
	}

	public String read() throws FileNotFoundException, IOException {
		String line = null;
		StringBuilder sb = new StringBuilder();
		
		// FileReader reads text files in the default encoding.
		FileReader fileReader = new FileReader(_fileName);

		// Always wrap FileReader in BufferedReader.
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		String delimiter = "\n";
		String tempDelimiter = "";
		
		while ((line = bufferedReader.readLine()) != null) {			
			sb.append(tempDelimiter + line);
			tempDelimiter = delimiter;
		}

		// Always close files.
		bufferedReader.close();
		
		return sb.toString();
	}

	private final String _fileName;
}

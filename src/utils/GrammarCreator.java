package utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import parser.Grammar;
import parser.NonTerminalSymbol;
import parser.ProductionRule;
import parser.Symbol;
import parser.SymbolList;
import parser.TerminalSymbol;

public class GrammarCreator {
	
	private String _grammar;
	
	public GrammarCreator(String grammar) {
		_grammar = grammar;
	}
	
	public Grammar create() {
		Scanner sc = new Scanner(_grammar);
		
		List<NonTerminalSymbol> V = new ArrayList<>(); 
		
		String nonTerm = sc.nextLine();		
		String[] nonTermArray = nonTerm.split(" ");
		ArrayList<String> nonTermList = new ArrayList<>(Arrays.asList(nonTermArray));
				
		for (String str : nonTermList) {
			V.add(new NonTerminalSymbol(str));
		}	
		
		List<TerminalSymbol> SIGMA = new ArrayList<>();
		
		String term = sc.nextLine();
		String[] termArray = term.split(" ");
		ArrayList<String> termList = new ArrayList<>(Arrays.asList(termArray));
		
		for (String str : termList) {
			SIGMA.add(new TerminalSymbol(str));
		}
		
		NonTerminalSymbol S = new NonTerminalSymbol(sc.nextLine());
		
		List<ProductionRule> P = new ArrayList<>();
		
		while (sc.hasNextLine()) {
			String prod = sc.nextLine();
			String[] prodArray = prod.split(" ");
			Symbol from = new NonTerminalSymbol(prodArray[0]);
			
			SymbolList symbolList = new SymbolList();
			
			String to = prodArray[1];
			String[] toArray = to.split("");
			ArrayList<String> toList = new ArrayList<>(Arrays.asList(toArray));
			
			for (String str : toList) {
				if (termList.contains(str)) {
					Symbol symb = new TerminalSymbol(str);
					symbolList.append(symb);
				}
				else if (nonTermList.contains(str)) {
					Symbol symb = new NonTerminalSymbol(str);
					symbolList.append(symb);
				}
				else {
					System.out.println("Can't find symbol " + str);
				}
			}
			
			P.add(new ProductionRule(from, symbolList));
		}
		
		sc.close();
		
		return new Grammar(V, SIGMA, S, P);
	}
	
	public static void main(String[] args) {
		SimpleTextFile fileReader = new SimpleTextFile("src/test/grammar.txt");
		try {
			GrammarCreator gc = new GrammarCreator(fileReader.read());
			gc.create();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
